instructions.o:
	g++ -c instructions.cpp
    
test: instructions.o debug/bits.o
	./tests/do.sh

debug/bits.o:
	g++ -c debug/bits.cpp

clean:
	find ./ -name "*.o" -exec rm {} \;