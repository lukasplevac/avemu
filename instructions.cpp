#include "instructions.h"
#include <stdio.h>

namespace instructions {
    /** math **/                         
    
    void add(unsigned char *instruction, unsigned char *registers, unsigned char *sreg) {
        unsigned char reg_num_a = get_register_5bits(instruction, 0);
        unsigned char reg_num_b = get_register_5bits(instruction, 1);
        
        /**    TMP scruct for borrow detect
         *    -------------------------------------------------------------------
         *    | bit |  9  |  8  |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
         *    -------------------------------------------------------------------
         *    | def |  1  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
         *    -------------------------------------------------------------------
         *    |     |  B  |  C  |                    DATA                       |
         *    -------------------------------------------------------------------
         *
         *    B   = borrow -> if is 0
         *    C   = carry  -> if is 1 and b is 1
         *    def = default
         */

        unsigned short int tmp = registers[reg_num_a];
        tmp += registers[reg_num_b];

        registers[reg_num_a] = tmp; //it do tmp & 0b0000000011111111
        
        // set c flag
        // if borrow carry is 1 too
        if (tmp & 0b100000000)
            sec(sreg);
        else
            clc(sreg);
    }

    void adc(unsigned char *instruction, unsigned char *registers, unsigned char *sreg) {
        unsigned char reg_num_a = get_register_5bits(instruction, 0);
        unsigned char reg_num_b = get_register_5bits(instruction, 1);

        /**    TMP scruct for borrow detect
         *    -------------------------------------------------------------------
         *    | bit |  9  |  8  |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
         *    -------------------------------------------------------------------
         *    | def |  1  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
         *    -------------------------------------------------------------------
         *    |     |  B  |  C  |                    DATA                       |
         *    -------------------------------------------------------------------
         *
         *    B   = borrow -> if is 0
         *    C   = carry  -> if is 1 and b is 1
         *    def = default
         */
        
        unsigned short int tmp = registers[reg_num_a];
        tmp += registers[reg_num_b] + (*sreg & 0b00000001);

        registers[reg_num_a] = tmp; //it do tmp & 0b0000000011111111

        //set c flag
        if (tmp & 0b100000000)
            sec(sreg);
        else
            clc(sreg);
    }

    void asr(unsigned char *instruction, unsigned char *registers, unsigned char *sreg) {
        unsigned char reg_num = get_register_5bits(instruction, 0);
        
        //set c flag
        if (registers[reg_num] & 0b00000001)
            sec(sreg);
        else
            clc(sreg);
        
        registers[reg_num] = registers[reg_num] >> 1;
    }

    /** logic **/

    void and_f(unsigned char *instruction, unsigned char *registers) {
        unsigned char reg_num_a = get_register_5bits(instruction, 0);
        unsigned char reg_num_b = get_register_5bits(instruction, 1);

        registers[reg_num_a] &= registers[reg_num_b];
    }

    void andi(unsigned char *instruction, unsigned char *registers_from_16) {
        unsigned char reg_num =       get_register_4bits(instruction);
        registers_from_16[reg_num] &= get_number(instruction);
    }


    /** memory **/

    void ldi(unsigned char *instruction, unsigned char *registers_from_16) {
        unsigned char reg_num =      get_register_4bits(instruction);
        registers_from_16[reg_num] = get_number(instruction);
    }

    void sec(unsigned char *sreg) {
        *sreg |= 0b00000001;
    }

    void clc(unsigned char *sreg) {
        *sreg &= 0b11111110;
    }

    /** core **/

    unsigned char get_register_5bits(unsigned char *instruction, unsigned char num) {
        if (num == 0)
            return ((instruction[0]        & 0b00000001) << 4) | ((instruction[1] >> 4) & 0b00001111);
        else
            return (((instruction[0] >> 1) & 0b00000001) << 4) | (instruction[1]        & 0b00001111);
    }

    unsigned char get_register_4bits(unsigned char *instruction) {
        return ((instruction[1] >> 4) & 0b00001111);
    }

    unsigned char get_number(unsigned char *instruction) {
        return (instruction[0] << 4) |  (instruction[1] & 0b00001111);
    }
}
