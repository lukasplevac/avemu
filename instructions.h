/** Architecture INFO
 *
 * Registers 0 - 31 => 8 bits 
 * Special registers:
 *     sreg: I T H S V N Z C
 *     pc  : 16bits address
 *     
 * memory address 16bits
 */

#ifndef instructions_h
#define instructions_h

/** Assembly definition **/
#define ldi_op    0b1110
#define adiw_ap   0b10010110 //todo
#define add_op    0b000111
#define adc_op    0b000111
#define and_op    0b001000
#define andi_op   0b0111
#define asr_op_h  0b1001010
#define asr_op_t  0b0101
#define bclr_op_h 0b100101001 //todo |
#define bclr_op_t 0b1000
#define sec_op    0b1001010000001000
#define clc_op    0b1001010010001000

namespace instructions {
    /*
     * core
     * ---------------------------------------------------------------------------------------- 
     */
    
    /**
     * Get address (5bits) of register from inscruction. In inscruction is two registers Rd and Rr.
     * 
     * @pre    in instruction must by 16 bit instruction in format XXXX XXrd dddd rrrr
     * @param  instruction pointer to unsigned char (array size: 2)
     * @param  num         unsigned char number of register in inscruction 0 or 1
     * @return unsigned char real address (0-31)
     */
    unsigned char get_register_5bits(unsigned char *instruction, unsigned char num);

    /**
     * Get address (4bits) of register from inscruction. Its register Rd from R16 to R31.
     * 
     * @pre    in instruction must by 16 bit instruction in format XXXX XXXX dddd XXXX
     * @param  instruction pointer to unsigned char (array size: 2)
     * @return unsigned char relative address (0-15) (it do dddd form instruction) you must address from register 16 (do output + 16)
     */
    unsigned char get_register_4bits(unsigned char *instruction);

    /**
     * Get constant (8bits) from inscruction.
     * 
     * @pre    in instruction must by 16 bit instruction in format XXXX KKKK XXXX KKKK
     * @param  instruction pointer to unsigned char (array size: 2)
     * @return unsigned char (0-255) constant
     */
    unsigned char get_number(unsigned char *instruction);

    /*
     * math
     * ----------------------------------------------------------------------------------------- 
     */
    
    /**
     * 16-bit Opcode: 0000 11rd dddd rrrr
     * Adds two registers without the C flag and places the result in the destination register Rd.
     * 
     * @pre    in instruction must by 16 bit instruction in format 0000 11rd dddd rrrr
     * @post   in register Rd is stored Rd + Rr
     * @param  instruction pointer to unsigned char (array size: 2)
     * @param  registers   pointer to unsigned char (array size: 32)
     * @param  sreg        pointer to unsigned char (array size: 1)
     * @return void
     */
    void add(unsigned char *instruction, unsigned char *registers, unsigned char *sreg);

    /**
     * 16-bit Opcode: 0001 11rd dddd rrrr
     * Adds two registers and the contents of the C flag and places the result in the destination register Rd.
     * 
     * @pre    in instruction must by 16 bit instruction in format 0001 11rd dddd rrrr
     * @post   in register Rd is stored Rd + Rr + C
     * @param  instruction pointer to unsigned char (array size: 2)
     * @param  registers   pointer to unsigned char (array size: 32)
     * @param  sreg        pointer to unsigned char (array size: 1)
     * @return void
     */
    void adc(unsigned char *instruction, unsigned char *registers, unsigned char *sreg);
    
    /**
     * 16-bit Opcode: 1001 010d dddd 0101
     * Shifts all bits in Rd one place to the right. Bit 7 is held constant.
     * Bit 0 is loaded into the C flag of the SREG. 
     * This operation effectively divides a signed value by two without changing its sign. 
     * The carry flag can be used to round the result.
     * 
     * @pre    in instruction must by 16 bit instruction in format 1001 010d dddd 0101
     * @post   in register Rd is stored Rd << 1
     * @param  instruction pointer to unsigned char (array size: 2)
     * @param  registers   pointer to unsigned char (array size: 32)
     * @param  sreg        pointer to unsigned char (array size: 1)
     * @return void
     */
    void asr(unsigned char *instruction, unsigned char *registers, unsigned char *sreg);

    /*
     * mem 
     * ---------------------------------------------------------------------------------------------
     */
    
    /**
     * 16-bit Opcode: 1110 KKKK dddd KKKK
     * Loads an 8 bit constant directly to register 16 to 31.
     * 
     * @pre    in instruction must by 16 bit instruction in format 1110 KKKK dddd KKKK
     * @post   in register Rd is stored K
     * @param  instruction pointer to unsigned char (array size: 2)
     * @param  registers   pointer to unsigned char (array size: 32)
     * @param  sreg        pointer to unsigned char (array size: 1)
     * @return void
     */
    void ldi(unsigned char *instruction, unsigned char *registers_from_16);

    /**
     * 16-bit Opcode: 1001 0100 0000 1000
     * Sets the Carry flag (C) in SREG (status register).
     * 
     * @post   in register SREG on 1st bit is stored 1
     * @param  sreg        pointer to unsigned char (array size: 1)
     * @return void
     */
    void sec(unsigned char *sreg);

    /**
     * 16-bit Opcode: 1001 0100 1000 1000
     * Clears the Carry flag (C) in SREG (status register).
     * 
     * @post   in register SREG on 1st bit is stored 0
     * @param  sreg        pointer to unsigned char (array size: 1)
     * @return void
     */
    void clc(unsigned char *sreg);

    /*
     * logic 
     * ------------------------------------------------------------------------------------------
     */

    /**
     * 16-bit Opcode: 0010 00rd dddd rrrr
     * Performs the logical AND between the contents of register Rd and register Rr and places the result in the destination register Rd.
     * 
     * @pre    in instruction must by 16 bit instruction in format 0010 00rd dddd rrrr
     * @post   in register Rd is stored Rd & Rr
     * @param  instruction pointer to unsigned char (array size: 2)
     * @param  registers   pointer to unsigned char (array size: 32)
     * @param  sreg        pointer to unsigned char (array size: 1)
     * @return void
     */
    void and_f(unsigned char *instruction, unsigned char *registers);
    
    /**
     * 16-bit Opcode: 0111 KKKK dddd KKKK
     * Performs the logical AND between the contents of register Rd and a constant and places the result in the destination register Rd.
     * 
     * @pre    in instruction must by 16 bit instruction in format 0010 00rd dddd rrrr
     * @post   in register Rd is stored Rd & K
     * @param  instruction pointer to unsigned char (array size: 2)
     * @param  registers   pointer to unsigned char (array size: 32)
     * @param  sreg        pointer to unsigned char (array size: 1)
     * @return void
     */
    void andi(unsigned char *instruction, unsigned char *registers_from_16);
}

#endif