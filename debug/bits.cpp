#include "bits.h"

namespace debug {
    void printBits(size_t const size, void const * const ptr) {
        unsigned char *b = (unsigned char*) ptr;
        unsigned char byte;
        int i, j;

        for (i=size-1;i>=0;i--)
        {
            for (j=7;j>=0;j--)
            {
                byte = (b[i] >> j) & 1;
                printf("%u", byte);
            }
        }
    }

    void print_mem(unsigned char *mem, int size) {
        printf("ADR\n");
        for (int i = 0; i < size; i++) {
            printf("%8X         HEX %8X     DEC %3d     BIN ", i, mem[i], mem[i]);
            printBits(1, &mem[i]);
            printf("\n");
        }
    }
}