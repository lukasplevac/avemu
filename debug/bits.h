#include <stdio.h>

namespace debug { 
    /* from https://stackoverflow.com/questions/111928/is-there-a-printf-converter-to-print-in-binary-format */
    /**
     * Print data as bits
     * @param size size of data type
     * @param prt pointer to varaible
     * @return void
     */
    void printBits(size_t const size, void const * const ptr);

    /**
     * Print memory data
     * @param mem char array (memory with 8bits cells)
     * @param size int size of memory in bytes (cells)
     * @return void
     */
    void print_mem(unsigned char *mem, int size);
}