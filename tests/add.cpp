#include <stdio.h>
#include "../instructions.h"
#include "../debug/bits.h"

#define tests_count 2

typedef struct {
   unsigned char value;
   unsigned char register_num;
   unsigned char sreg;
} target_t;

int main() {
    unsigned char registers[32] = {
        10, 5, 255, 240
    };

    unsigned char sreg = 0;

    unsigned char testprog[] = {
        0b00001100, 0b00000001, //ADD R0,  R1
        0b00001100, 0b00100011, //ADD R2,  R3
    };

    target_t targets[] = {
        {15,  0, 0},
        {239, 2, 1},
    };

    for (int i = 0; i < tests_count; i++) {
        instructions::add(&testprog[i*2], registers, &sreg);

        if (registers[targets[i].register_num] != targets[i].value || sreg != targets[i].sreg) {
            printf("add test fail with input inscriction: ");
            
            debug::printBits(1, &testprog[i*2]);

            printf(" ");

            debug::printBits(1, &testprog[i*2 + 1]);
            
            printf("\n\nregisters dump: \n");
            
            debug::print_mem(registers, 32);

            return 1;
        }
    }

    return 0;
}