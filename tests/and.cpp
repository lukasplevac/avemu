#include <stdio.h>
#include "../instructions.h"
#include "../debug/bits.h"

#define tests_count 4

typedef struct {
   unsigned char value;
   unsigned char register_num;
} target_t;

int main() {
    unsigned char registers[32] = {
        0b00000000,
        0b10101010,
        0b01010101,
        0b11111111,
        0b01110101
    }; //define registers

    unsigned char testprog[] = {
        0b00100000, 0b00000001, //AND R0, R1
        0b00100000, 0b00010011, //AND R1, R3
        0b00100000, 0b00110011, //AND R3, R3
        0b00100000, 0b01000010  //AND R4, R2
    };

    target_t targets[] = {
        {0b00000000, 0},
        {0b10101010, 1},
        {0b11111111, 3},
        {0b01010101, 4}
    };

    for (int i = 0; i < tests_count; i++) {
        instructions::and_f(&testprog[i*2], registers);

        if (registers[targets[i].register_num] != targets[i].value) {
            printf("and test fail with input inscriction: ");
            
            debug::printBits(1, &testprog[i*2]);

            printf(" ");

            debug::printBits(1, &testprog[i*2 + 1]);
            
            printf("\n\nregisters dump: \n");
            
            debug::print_mem(registers, 32);

            return 1;
        }
    }

    return 0;
}
