#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

cd $DIR

#compile all tests
EXT=cpp
for i in *; do
    if [ "${i}" != "${i%.${EXT}}" ];then
        g++ ../instructions.o ../bits.o $i -o $i.o || exit 1
    fi
done

#run tests
EXT=o
for i in *; do
    if [ "${i}" != "${i%.${EXT}}" ];then
        echo "testing $i"
        ./$i || exit 1
    fi
done