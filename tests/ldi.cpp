#include <stdio.h>
#include "../instructions.h"
#include "../debug/bits.h"

#define tests_count 4

typedef struct {
   unsigned char value;
   unsigned char register_num;
} target_t;

int main() {
    unsigned char registers[32]; //define registers

    unsigned char testprog[] = {
        0b11101010, 0b00000101, //LDI R16, 165 (0b10100101)
        0b11100000, 0b11110000, //LDI R31, 0   (0b00000000)
        0b11101111, 0b01001111, //LDI R20, 255 (0b11111111)
        0b11100011, 0b10111000, //LDI R27, 56  (0b00111000)
    };

    target_t targets[] = {
        {0b10100101, 16},
        {0b00000000, 31},
        {0b11111111, 20},
        {0b00111000, 27}
    };

    for (int i = 0; i < tests_count; i++) {
        instructions::ldi(&testprog[i*2], &registers[16]);

        if (registers[targets[i].register_num] != targets[i].value) {
            printf("ldi test fail with input inscriction: ");
            
            debug::printBits(1, &testprog[i*2]);

            printf(" ");

            debug::printBits(1, &testprog[i*2 + 1]);
            
            printf("\n\nregisters dump: \n");
            
            debug::print_mem(registers, 32);

            return 1;
        }
    }

    return 0;
}