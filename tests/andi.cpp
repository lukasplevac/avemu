#include <stdio.h>
#include "../instructions.h"
#include "../debug/bits.h"

#define tests_count 4

typedef struct {
   unsigned char value;
   unsigned char register_num;
} target_t;

int main() {
    unsigned char registers[32]; //define registers

    registers[16] = 0b01101011;
    registers[17] = 0b10101010;
    registers[18] = 0b01010101;
    registers[19] = 0b11111111;

    unsigned char testprog[] = {
        0b01110110, 0b00001011, //ANDI R16, 0b01101011
        0b01110000, 0b00010000, //ANDI R17, 0b00000000
        0b01111111, 0b00101111, //ANDI R18, 0b11111111
        0b01110101, 0b00110101  //ANDI R19, 0b01010101
    };

    target_t targets[] = {
        {0b01101011, 16},
        {0b00000000, 17},
        {0b01010101, 18},
        {0b01010101, 19}
    };

    for (int i = 0; i < tests_count; i++) {
        instructions::andi(&testprog[i*2], &registers[16]);

        if (registers[targets[i].register_num] != targets[i].value) {
            printf("andi test fail with input inscriction: ");
            
            debug::printBits(1, &testprog[i*2]);

            printf(" ");

            debug::printBits(1, &testprog[i*2 + 1]);
            
            printf("\n\nregisters dump: \n");
            
            debug::print_mem(registers, 32);

            return 1;
        }
    }

    return 0;
}
