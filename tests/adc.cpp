#include <stdio.h>
#include "../instructions.h"
#include "../debug/bits.h"

#define tests_count 2

typedef struct {
   unsigned char value;
   unsigned char register_num;
   unsigned char sreg;
} target_t;

int main() {
    unsigned char registers[32] = {
        0, 156, //156 
        1, 156  //413
    };

    unsigned char sreg = 0;

    unsigned char testprog[] = {
        0b00011100, 0b00010011, //ADC R1,  R3
        0b00011100, 0b00000010, //ADC R0,  R2
    };

    target_t targets[] = {
        {56,  1, 1},
        {2,   0, 0},
    };

    for (int i = 0; i < tests_count; i++) {
        instructions::adc(&testprog[i*2], registers, &sreg);

        if (registers[targets[i].register_num] != targets[i].value || sreg != targets[i].sreg) {
            printf("add test fail with input inscriction: ");
            
            debug::printBits(1, &testprog[i*2]);

            printf(" ");

            debug::printBits(1, &testprog[i*2 + 1]);
            
            printf("\n\nregisters dump: \n");
            
            debug::print_mem(registers, 32);

            return 1;
        }
    }

    return 0;
}