/**
 * Lot of bugs only for testing the transitional version
 * it is design as it should look like
 */

#include "instructions.h"
#include "debug/bits.h"
#include <stdio.h>
#include <stdlib.h>

unsigned char prog_1[] = {
    0b11101010, 0b00000101, //LDI R16, 165 (0b10100101)
    0b11100011, 0b10111000, //LDI R27, 56  (0b00111000)
    0b00011111, 0b10110000  //ADD R27, R16
};

namespace avemu {

    typedef struct {
        unsigned char *memory;
        unsigned char (*get_prog_byte)(unsigned short pointer);
    } instance_t;

    typedef struct {
        instance_t *instance;
        unsigned char count = 0;
    } instances_t;

    class cpu {
        public:
            cpu() {
                this->instances.instance = (instance_t*) malloc(1);
            }

            ~cpu() {
                for (unsigned char i = 0; i < this->instances.count; i++) {
                    free(this->instances.instance[i].memory);
                }

                free(this->instances.instance);
            }

            bool run(short unsigned memsize, unsigned char (*get_prog_byte)(unsigned short pointer)) {
                //can I create instance?
                if (this->instances.count >= 255)
                    return false;

                this->instances.count++;

                if (!this->inrealloc())
                    return false;

                //try alloc memory
                unsigned char *memory = (unsigned char*) malloc(memsize);
                if (memory == NULL) {
                    this->instances.count--;
                    this->inrealloc();
                    return false;
                }

                this->instances.instance[this->instances.count - 1].memory = memory;
                this->instances.instance[this->instances.count - 1].get_prog_byte = get_prog_byte;
                //for test
                this->instances.instance[this->instances.count - 1].memory[32] = 0;
            }

            void tick() {
                for (unsigned char i = 0; i < this->instances.count; i++) {
                    this->parse(this->instances.instance[i].memory, this->instances.instance[i].get_prog_byte);
                }
            }

            void debug() {
                for (unsigned char i = 0; i < this->instances.count; i++) {
                    debug::print_mem(this->instances.instance[i].memory, 34);
                }
            }

        private:
            instances_t instances;

            bool inrealloc() {
                //todo: check NULL pointer
                this->instances.instance = (instance_t*) realloc(this->instances.instance, this->instances.count * sizeof(instances_t));

                return true;
            }

            void parse(unsigned char *memory, unsigned char (*get_prog_byte)(unsigned short pointer)) {
                unsigned char op[] = { get_prog_byte(memory[32]), 
                                       get_prog_byte(memory[32] + 1)
                                     };

                if (((op[0] >> 4) & 0b00001111) == ldi_op) {
                    instructions::ldi(op, &memory[16]);
                    printf("tick ldi\n");
                } else if (((op[0] >> 2) & 0b00111111) == add_op) {
                    instructions::add(op, memory, &memory[33]);
                    printf("tick add\n");
                }

                memory[32] += 2;
            }
    };
}


unsigned char get_prog_byte(unsigned short pointer) {
    return prog_1[pointer];
}

int main() {
    avemu::cpu cpu;

    cpu.run(34, get_prog_byte);
    cpu.run(34, get_prog_byte);
    cpu.run(34, get_prog_byte);
    
    cpu.tick();
    cpu.tick();
    cpu.tick();

    cpu.debug();

    return 0;
}